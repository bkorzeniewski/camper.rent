<?php

declare(strict_types=1);

namespace App\Admin;

use App\Form\Admin\ImageType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\MediaBundle\Form\Type\MediaType;

final class VehicleAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('id')
            ->add('year')
            ->add('numberPeople')
            ->add('location')
            ->add('description')
            ->add('title')
            ->add('slug')
            ->add('enable')
            ->add('accepted')
            ->add('price')
            ->add('createdAt')
            ->add('updatedAt');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('year')
            ->add('numberPeople')
            ->add('location')
            ->add('description')
            ->add('title')
            ->add('slug')
            ->add('enable')
            ->add('accepted')
            ->add('price')
            ->add('createdAt')
            ->add('updatedAt')
            ->add(ListMapper::NAME_ACTIONS, null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('vehicleType')
            ->add('brand', ModelType::class)
            ->add('brandModel', ModelType::class)
            ->add('title')
            ->add('description')
            ->add('location')
            ->add('year')
            ->add('numberPeople')
            ->add('price')
            ->add('enable')
            ->add('title')
            ->add('accepted')
            ->add('images', CollectionType::class, [
                'entry_type' => ImageType::class,
                'entry_options' => [
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'mapped' => true,
                'required' => true,
            ]
            );
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('year')
            ->add('numberPeople')
            ->add('location')
            ->add('description')
            ->add('title')
            ->add('slug')
            ->add('enable')
            ->add('accepted')
            ->add('price')
            ->add('createdAt')
            ->add('updatedAt');
    }
}
