<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class ApiController extends AbstractFOSRestController
{
    /**
     * @Rest\Options("/api/users")
     */
    public function index(): View
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        return $this->view(['form' => $form]);
    }
}
