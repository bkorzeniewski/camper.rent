<?php

namespace App\Entity\Attribute;

use App\Entity\Vehicle\VehicleAttribute;
use App\Repository\Attribute\AttributeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=AttributeRepository::class)
 * @Gedmo\Loggable
 */
class Attribute
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity=AttributeGroup::class, inversedBy="attributes")
     */
    private $attributeGroup;

    /**
     * @ORM\OneToMany(targetEntity=VehicleAttribute::class, mappedBy="attribute")
     */
    private $vehicleAttributes;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $enable = true;

    public function __construct()
    {
        $this->vehicleAttributes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function isEnable(): bool
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable(bool $enable): void
    {
        $this->enable = $enable;
    }


    public function getAttributeGroup(): ?AttributeGroup
    {
        return $this->attributeGroup;
    }

    public function setAttributeGroup(?AttributeGroup $attributeGroup): self
    {
        $this->attributeGroup = $attributeGroup;

        return $this;
    }

    /**
     * @return Collection|VehicleAttribute[]
     */
    public function getVehicleAttributes(): Collection
    {
        return $this->vehicleAttributes;
    }

    public function addVehicleAttribute(VehicleAttribute $vehicleAttribute): self
    {
        if (!$this->vehicleAttributes->contains($vehicleAttribute)) {
            $this->vehicleAttributes[] = $vehicleAttribute;
            $vehicleAttribute->setAttribute($this);
        }

        return $this;
    }

    public function removeVehicleAttribute(VehicleAttribute $vehicleAttribute): self
    {
        if ($this->vehicleAttributes->removeElement($vehicleAttribute)) {
            // set the owning side to null (unless already changed)
            if ($vehicleAttribute->getAttribute() === $this) {
                $vehicleAttribute->setAttribute(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
