<?php

namespace App\Entity\Equipment;

use App\Entity\Vehicle\Vehicle;
use App\Entity\Vehicle\VehicleType;
use App\Repository\Equipment\EquipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=EquipmentRepository::class)
 * @Gedmo\Loggable
 */
class Equipment
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    private string $slug;

    /**
     * @ORM\ManyToMany(targetEntity=VehicleType::class, mappedBy="equipments", cascade={"persist"})
     */
    private Collection $vehicleTypes;

    /**
     * @ORM\ManyToMany(targetEntity=Vehicle::class, mappedBy="equipments")
     */
    private Collection $vehicles;

    /**
     * @ORM\ManyToOne(targetEntity=EquipmentGroup::class, inversedBy="equipments")
     */
    private ?EquipmentGroup $equipmentGroup;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $enable = true;

    public function __construct()
    {
        $this->vehicleTypes = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function isEnable(): bool
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable(bool $enable): void
    {
        $this->enable = $enable;
    }

    /**
     * @return Collection|VehicleType[]
     */
    public function getVehicleTypes(): Collection
    {
        return $this->vehicleTypes;
    }

    public function addVehicleType(VehicleType $vehicleType): self
    {
        if (!$this->vehicleTypes->contains($vehicleType)) {
            $this->vehicleTypes[] = $vehicleType;
            $vehicleType->addEquipment($this);
        }

        return $this;
    }

    public function removeVehicleType(VehicleType $vehicleType): self
    {
        if ($this->vehicleTypes->removeElement($vehicleType)) {
            $vehicleType->removeEquipment($this);
        }

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles[] = $vehicle;
            $vehicle->addEquipment($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicles->removeElement($vehicle)) {
            $vehicle->removeEquipment($this);
        }

        return $this;
    }

    public function getEquipmentGroup(): ?EquipmentGroup
    {
        return $this->equipmentGroup;
    }

    public function setEquipmentGroup(?EquipmentGroup $equipmentGroup): self
    {
        $this->equipmentGroup = $equipmentGroup;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

}
