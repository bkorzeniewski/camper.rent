<?php

declare(strict_types=1);

namespace App\Entity\Media;

use App\Entity\Vehicle\Vehicle;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\MediaBundle\Entity\BaseMedia;

/**
 * @ORM\Entity
 * @ORM\Table(name="media")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\Loggable
 */
class Media extends BaseMedia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups(groups={"sonata_api_read", "sonata_api_write", "sonata_search"})
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity=MediaGalleryHasMedia::class,
     *     mappedBy="media", cascade={"persist"}, orphanRemoval=false
     * )
     *
     * @var MediaGalleryHasMedia[]
     */
    protected $galleryHasMedias;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $enable = true;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicle::class, inversedBy="images")
     */
    private $vehicle;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $defaultImage = false;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(): void
    {
        parent::prePersist();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(): void
    {
        parent::preUpdate();
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getDefaultImage(): ?bool
    {
        return $this->defaultImage;
    }

    public function setDefaultImage(bool $defaultImage): self
    {
        $this->defaultImage = $defaultImage;

        return $this;
    }
}
