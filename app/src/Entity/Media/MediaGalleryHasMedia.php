<?php

declare(strict_types=1);

namespace App\Entity\Media;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\MediaBundle\Entity\BaseGalleryHasMedia;

/**
 * @ORM\Entity
 * @ORM\Table(name="media__gallery_media")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\Loggable
 */
class MediaGalleryHasMedia extends BaseGalleryHasMedia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups(groups={"sonata_api_read", "sonata_api_write", "sonata_search"})
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Media::class,
     *     inversedBy="galleryHasMedias", cascade={"persist"}
     * )
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var Media
     */
    protected $media;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=MediaGallery::class,
     *     inversedBy="galleryHasMedias", cascade={"persist"}
     * )
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var MediaGallery
     */
    protected $gallery;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(): void
    {
        parent::prePersist();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(): void
    {
        parent::preUpdate();
    }
}
