<?php

namespace App\Entity;

use App\Repository\TimetableRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=TimetableRepository::class)
 * @Gedmo\Loggable
 */
class Timetable
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $datetimeFrom;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $datetimeTo;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $enable = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetimeFrom(): ?\DateTimeImmutable
    {
        return $this->datetimeFrom;
    }

    public function setDatetimeFrom(\DateTimeImmutable $datetimeFrom): self
    {
        $this->datetimeFrom = $datetimeFrom;

        return $this;
    }

    public function getDatetimeTo(): ?\DateTimeImmutable
    {
        return $this->datetimeTo;
    }

    public function setDatetimeTo(\DateTimeImmutable $datetimeTo): self
    {
        $this->datetimeTo = $datetimeTo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnable(): bool
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable(bool $enable): void
    {
        $this->enable = $enable;
    }

}
