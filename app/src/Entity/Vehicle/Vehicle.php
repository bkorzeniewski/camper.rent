<?php

namespace App\Entity\Vehicle;

use App\Entity\Brand\Brand;
use App\Entity\Brand\BrandModel;
use App\Entity\Equipment\Equipment;
use App\Entity\Media\Media;
use App\Repository\Vehicle\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=VehicleRepository::class)
 * @Gedmo\Loggable
 */
class Vehicle
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected int $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $year;

    /**
     * @ORM\Column(type="integer")
     */
    protected $numberPeople;

    /**
     * @ORM\ManyToOne(targetEntity=VehicleType::class, inversedBy="vehicles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vehicleType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=Equipment::class, inversedBy="vehicles")
     */
    private $equipments;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="vehicles")
     * @ORM\JoinColumn(nullable=false)
     */
    private Brand $brand;

    /**
     * @ORM\ManyToOne(targetEntity=BrandModel::class, inversedBy="vehicles")
     * @ORM\JoinColumn(nullable=false)
     */
    private BrandModel $brandModel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=255, unique=true)
     */
    private string $slug;

    /**
     * @ORM\OneToMany(targetEntity=VehicleAttribute::class, mappedBy="vehicle")
     */
    private $vehicleAttributes;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $enable = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $accepted = false;

    /**
     * @ORM\OneToMany(targetEntity=Media::class, mappedBy="vehicle", cascade={"persist"})
     */
    private $images;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    public function __construct()
    {
        $this->equipments = new ArrayCollection();
        $this->vehicleAttributes = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand(Brand $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return BrandModel
     */
    public function getBrandModel(): BrandModel
    {
        return $this->brandModel;
    }

    /**
     * @param BrandModel $brandModel
     */
    public function setBrandModel(BrandModel $brandModel): void
    {
        $this->brandModel = $brandModel;
    }


    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getNumberPeople(): ?int
    {
        return $this->numberPeople;
    }

    public function setNumberPeople(int $numberPeople): self
    {
        $this->numberPeople = $numberPeople;

        return $this;
    }

    public function getVehicleType(): ?VehicleType
    {
        return $this->vehicleType;
    }

    public function setVehicleType(?VehicleType $vehicleType): self
    {
        $this->vehicleType = $vehicleType;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Equipment[]
     */
    public function getEquipments(): Collection
    {
        return $this->equipments;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
        }

        return $this;
    }

    public function removeEquipment(Equipment $equipment): self
    {
        $this->equipments->removeElement($equipment);

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }


    /**
     * @return Collection|VehicleAttribute[]
     */
    public function getVehicleAttributes(): Collection
    {
        return $this->vehicleAttributes;
    }

    public function addVehicleAttribute(VehicleAttribute $vehicleAttribute): self
    {
        if (!$this->vehicleAttributes->contains($vehicleAttribute)) {
            $this->vehicleAttributes[] = $vehicleAttribute;
            $vehicleAttribute->setVehicle($this);
        }

        return $this;
    }

    public function removeVehicleAttribute(VehicleAttribute $vehicleAttribute): self
    {
        if ($this->vehicleAttributes->removeElement($vehicleAttribute)) {
            // set the owning side to null (unless already changed)
            if ($vehicleAttribute->getVehicle() === $this) {
                $vehicleAttribute->setVehicle(null);
            }
        }

        return $this;
    }

    public function isEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * @return Collection|Media[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Media $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setVehicle($this);
        }

        return $this;
    }

    public function removeImage(Media $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getVehicle() === $this) {
                $image->setVehicle(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
