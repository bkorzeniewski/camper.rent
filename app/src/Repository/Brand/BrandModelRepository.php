<?php

namespace App\Repository\Brand;

use App\Entity\Brand\BrandModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BrandModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method BrandModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method BrandModel[]    findAll()
 * @method BrandModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrandModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BrandModel::class);
    }

    // /**
    //  * @return BrandModel[] Returns an array of BrandModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BrandModel
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
