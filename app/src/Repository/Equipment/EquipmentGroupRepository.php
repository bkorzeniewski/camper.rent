<?php

namespace App\Repository\Equipment;

use App\Entity\Equipment\EquipmentGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipmentGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipmentGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipmentGroup[]    findAll()
 * @method EquipmentGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipmentGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipmentGroup::class);
    }

    // /**
    //  * @return EquipmentGroup[] Returns an array of EquipmentGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EquipmentGroup
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
