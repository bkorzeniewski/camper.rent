<?php

namespace App\Repository\Vehicle;

use App\Entity\Vehicle\VehicleAttribute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VehicleAttribute|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehicleAttribute|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehicleAttribute[]    findAll()
 * @method VehicleAttribute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleAttributeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehicleAttribute::class);
    }

    // /**
    //  * @return VehicleAttribute[] Returns an array of VehicleAttribute objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VehicleAttribute
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
